from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Variables

list_of_films = []
list_of_films_lenght = 0

# driver and browser setup
driver = webdriver.Firefox()
driver.implicitly_wait(10)
driver.get("https://www.rottentomatoes.com/browse/in-theaters/")

# search only best films
driver.find_element_by_xpath("//*[@id='tomatometerFilter']//button[@data-toggle='dropdown']").click()
driver.find_element_by_id("cf-checkbox").click()

# get all names of films results

# return the text Showing num1 of num2
text = driver.find_element_by_xpath("//div[@id='count-link']/span").text

# the split return a list by delimitator blank space and get the element with index = 1, this element is the first number in string Showing num1 of num2
list_of_films_lenght = int(text.split(" ")[1])

for item in range(2, list_of_films_lenght, 1):
    film = driver.find_element_by_xpath("//div/div[@class='mb-movies']/div["+ str(item) +"]//h3").text
    list_of_films.append(film)

driver.quit()

# export file

file = open("target/list_of_films.txt", "w")
for film in list_of_films:
    file.write(film + "\n")
file.close()
